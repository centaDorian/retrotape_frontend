
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '', component: () => import('pages/Index.vue'),
        path: 'movies', component: () => import('pages/Movies.vue')
      }
    ]
  },
  // {
  //   path: '/movies',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [
  //     { path: 'moviesByGenres', component: () => import('pages/Index.vue') },
  //     { path: ':id', component: () => import('pages/Index.vue') }

  //   ]
  // },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('layouts/MainLayout.vue')
  },

  {
    path: '/movies',
    component: () => import('../pages/Movies.vue')
  },
]

export default routes
