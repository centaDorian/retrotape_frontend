

export default {
     getParams: (pagination) =>
    {
      var query = pagination;
      query.start = (pagination.currentPage-1)*pagination.limit;
      return query;
    }
};